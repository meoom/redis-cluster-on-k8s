#!/bin/bash

if [ ! -f "/data/nodes.conf" ]; then
    echo "nodes.conf doesn't exist,first start"
    redis-server /etc/redis/redis.conf --cluster-announce-ip $MY_POD_IP &
    sleep 2s;
    /usr/local/bin/writeip.sh
    ping -i 10 localhost
fi

/usr/local/bin/writeip.sh

echo "check nodes.conf"
cat /data/nodes.conf|grep -E "master|slave"|awk '{print $1}'|xargs -i /usr/local/bin/checkip.sh {}

if [ $? -eq 0 ]; then
    echo "done nodes.conf"
    redis-server /etc/redis/redis.conf --cluster-announce-ip $MY_POD_IP
    ping -i 10 localhost
else
    echo "abort on error"
fi
